# Course outline
A preliminary outline of the course can be accessed [here](http://uc-media.rhi.hi.is/tmp/xwplx/Course_Outline_%28preliminary%29.pdf).

# Assessment
The final grade will be based on the following factors:
- 30% - Assignments (Bi-weekly)
- 20% - Presentation by student (~ 30 - 40 minutes)
- 50% - Exams (mid-term + final)

# Textbooks
This course covers material that can be found in the following textbooks:
- [Mo, van den Bosch & White (MBW) - *Galaxy Formation and Evolution*](https://books.google.is/books/about/Galaxy_Formation_and_Evolution.html?id=Zj7fDU3Z4wsC&redir_esc=y)
  - This is the main textbook of the course. It should contain all the material (with some exceptions).
- [Kolb & Turner (KT) - *The early Universe*](https://books.google.is/books/about/The_Early_Universe.html?id=E58_BAAAQBAJ&redir_esc=y)
  - Has a good description of the role of thermodynamics, non-equilibrium dynamics and nucleosynthesis in cosmology.
- [Binney and Treamaine (BT) - *Galactic Dynamics*](https://books.google.is/books/about/Galactic_Dynamics.html?id=6mF4CKxlbLsC&redir_esc=y)
  - An advanced textbook, suitable for the 2nd half of the course.
