# Lecture 1

## Topics covered:
The Standard Cosmological Model / The Background Universe
  - The Cosmological Principle
  - Some evidence for isotropy/homogeneity of the universe
  - Cosmic Microwave Background (CMB)
  - The expansion of the Universe, relating to the CMB

  
