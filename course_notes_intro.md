# Selected topics in Modern Astrophysics

This blog will be updated with notes based on the lectures in [EÐL022M](https://ugla.hi.is/kennsluskra/index.php?tab=nam&chapter=namskeid&id=71005220196), a course in modern astrophysics, taught during the autumn of 2019.

A rough overview of the course, along with textbooks and grading criteria, can be found [here](course_overview.md).

Click [here](lectures/lecture01.md) to start from the first lecture.
